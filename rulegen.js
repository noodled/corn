/*
The MIT License (MIT)

Copyright (c) 2014 Stian Jahr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


An one more thing: Please don't laught at my code, I suck in web development...
 */



function genFiledata(){
	return " filedata; "
}

function genContent(row){
	/*
	To get all the elements starting with "jander" you should use:
	$("[id^=jander]")

	To get those that end with "jander"
	$("[id$=jander]")
	*/

	console.log("Adding content");
	//console.log(row);
	//console.log(row.id);
	//r = $("#" + row.id);
	//console.log("r:");
	//console.log(r);
	//t = r.find("[id^=r_text]");
	//console.log("t:");

	//console.log(t[0].value);


	console.log(row)


	// NEGATIVE MATCH
	neg = "";

	// CONTENT

	if ((($("#" + row.id).find("[id^=r_negative]") )[0]).checked) {
		neg = "!";
	}

	r_content = " content:" + neg + "\"" + ($("#" + row.id).find("[id^=r_text]") )[0].value + "\";";

	// DATATYPE
	r_datatype = ($("#" + row.id).find("[id^=r_datatype]") )[0].value;

	console.log("content: " + r_content);
	console.log(r_datatype);
	if (r_datatype != "-not set-") {
		r_datatype = " " + r_datatype + ";"
	}

	else {
		r_datatype = "";
	}

	// FLAGS
	r_flags = "";

	if((($("#" + row.id).find("[id^=r_nocase]") )[0]).checked) {
		r_flags += " nocase;";
	}

	if((($("#" + row.id).find("[id^=r_rawbytes]") )[0]).checked) {
		r_flags += " rawbyes;";
	}

	if((($("#" + row.id).find("[id^=r_fastpattern]") )[0]).checked) {
		r_flags += " fast_pattern;";
	}

	// POSITION

	r_pos = "";

	if((($("#" + row.id).find("[id^=r_offset]") )[0].value) != "") {
		r_pos += " offset:" + $("#" + row.id).find("[id^=r_offset]")[0].value + ";";
	}

	if((($("#" + row.id).find("[id^=r_depth]") )[0].value) != "") {
		r_pos += " depth:" + $("#" + row.id).find("[id^=r_depth]")[0].value + ";";
	}

	if((($("#" + row.id).find("[id^=r_distance]") )[0].value) != "") {
		r_pos += " distance:" + $("#" + row.id).find("[id^=r_distance]")[0].value + ";";
	}

	if((($("#" + row.id).find("[id^=r_within]") )[0].value) != "") {
		r_pos += " within:" + $("#" + row.id).find("[id^=r_within]")[0].value + ";";
	}



	//console.log(row.children["id^=r_text]");

	//console.log(row.find());


	//msg = row.find('[id^=r_text')


	return r_content + r_datatype + r_flags + r_pos;

}


function genHeader(){

	header = $("#f_action").val() + " " + $("#f_proto").val() +
		" " + $("#f_sip").val() + " " + $("#f_sport").val() + " " +
		$("#f_dir").val() + " " + $("#f_dip").val() + " " +
		$("#f_dport").val() + " ";

	return header;
}


function genPreContent(){
	msg = "msg:\"" + $("#f_msg").val()   + "\";"

	flow = "";
	flow_s = "";
	flow_post = "";

	console.log($("#f_est"))

	if ($("#f_est")[0].checked) {
		flow = " flow:"
		flow_s = "established"
	}

	if ( $("#f_flowdir").val() != "-not set-"){
		flow = " flow:"
		if (flow_s != "") { flow_s += ","; }
		flow_s += $("#f_flowdir").val()
	}

	if (flow != "" || flow_s != "") { flow_post = ";" }

	return  msg + flow + flow_s + flow_post;
}

function genFooter(){

	footer = "";

	if ($("#f_classtype").val() != "-not set-"){
		footer += " classtype:" + $("#f_classtype").val() + ";";
	}

	if ($("#f_priority").val() != ""){
		footer += " priority:" + $("#f_priority").val() + ";";
	}



	footer += " sid:" + $("#f_sid").val() + ";";

	if ($("#f_rev").val() != "") {
		footer += " rev:" + $("#f_rev").val() + ";"
	}

	else {
		footer += " rev:1;"
	}

	if ($("#f_gen").val() != "") {
		footer += " gen:" + $("#f_gen").val() + ";"
	}


	return footer + ")";
}

function genDnsContent(domain){
	parts = domain.split('.');
	r_domain = "";
	parts.forEach(function(p){
		var h = p.length.toString(16)
		h = h.length === 1 ? '0' + h : h
		r_domain += '|' + h + '|' + p;
	})

	return r_domain;
}

function genDns(row){
	//r_content = " content:" + neg + "\"" + ($("#" + row.id).find("[id^=r_text]") )[0].value + "\";";


	r_suffix = "";

	b_q = $("#" + row.id).find("[id^=r_query]")[0].checked;
	b_a = $("#" + row.id).find("[id^=r_answer]")[0].checked

	r_q_or_a = "";


	// Add |00| if not "Allow suffix" is not set
	if (! ($("#" + row.id).find("[id^=r_suffix]") )[0].checked) {
		r_suffix = "|00|"
	}
	else {
		r_suffix = "";
	}

	// If both q and a is checked, we dont specify the Q/A-bit
	if ( b_q && b_a ) {
		r_q_or_a = "";
	}

	// If query
	else if (b_q) {
		//xxxx
		r_q_or_a = " byte_test:1,!&,0xF8,2;" ;
	}

	else if (b_a) {
		r_q_or_a = " byte_test:1,!&,0xF8,0; ";
	}


	r_content = " content:" + "\"" + genDnsContent(  ($("#" + row.id).find("[id^=r_domain]") )[0].value) + r_suffix + "\";" + r_q_or_a;
	return r_content;
}



function genCustom(row) {
	return " " + ($("#" + row.id).find("[id^=r_custom]") )[0].value
}

function genPcre(row) {

	neg = "";
	if ((($("#" + row.id).find("[id^=r_pcreneg]") )[0]).checked) {
		neg = "!";
	}

	return " pcre:" + neg + "\"" + ($("#" + row.id).find("[id^=r_pcre]") )[0].value + "\";";
}


// Generate the rule from htmls elements
function genrule() {

	//var r_action = $("#f_action").val();
	//var r_proto = $("#f_proto").val();
	console.log("run");


	var r_header = genHeader();

	var r_pre_content = genPreContent();

	var r_footer = genFooter();

	var r_body = "";

	$("#t_contents tbody tr").each(function(item,col)
	{
		//console.log("item: " + item);console.log(col);
		//console.log(col.className);
		if(col.className.indexOf("tr_filedatarow") > -1) {
			//console.log("Adding filedata");
			r_body += genFiledata();
		}

		else if (col.className.indexOf("tr_dnsrow") > -1) {
			//console.log("Adding contet");
			r_body += genDns(col);
		}

		else if (col.className.indexOf("tr_contentrow") > -1) {
			//console.log("Adding contet");
			r_body += genContent(col);
		}

		else if (col.className.indexOf("tr_customrow") > -1) {
			//console.log("Adding contet");
			r_body += genCustom(col);
		}

		else if (col.className.indexOf("tr_pcrerow") > -1){
			r_body += genPcre(col);
		}

	});



	var tmp = $('#t_contents tbody').html();

	var r_out = r_header + "(" + r_pre_content;

	r_out += r_body + r_footer;

	$("#generated_rule").val(r_out);


}

// Global var that holds the next ID of a rule
var nextRowId = 1;

function addContentRow() {
	console.log("Adding row");
	var content_row = '\
                    <tr id="cr_' + nextRowId + '" class=tr_contentrow">\
                        <td><label for="r_text_' + nextRowId + '">Content: </label><br><input type="text" id="r_text_' + nextRowId + '" class="req"></td>\
                        <td>\
							<label for="r_datatype_' + nextRowId + '">Datatype:</label><br>\
							<select id="r_datatype_' + nextRowId + '" class="nonreq">\
								<option>-not set-</option><option>http_client_body</option><option>http_cookie</option><option>http_raw_cookie</option><option>http_header</option><option>http_raw_header</option><option>http_method</option><option>http_uri</option><option>http_raw_uri</option><option>http_stat_code</option><option>http_stat_msg</option>\
							</select>\
                        </td>\
                        <td>\
							<label for="r_flags_' + nextRowId + '">Flags:</label><br>\
							<div id="r_flags_' + nextRowId + '" class="nonreq">\
							\
							<input type="checkbox" id="r_negative_' + nextRowId + '"><label for="r_negative_' + nextRowId + '">negative content</label><br>\
							<input type="checkbox" id="r_nocase_' + nextRowId + '"><label for="r_nocase_' + nextRowId + '">nocase</label><br>\
							<input type="checkbox" id="r_rawbytes_' + nextRowId + '"><label for="r_rawbytes_' + nextRowId + '">rawbytes</label><br>\
							<input type="checkbox" id="r_fastpattern_' + nextRowId + '"><label for="r_fastpattern_' + nextRowId + '">fast_pattern</label>\
							</div>\
						</td>\
						<td>\
							<label>Abs positioning</label><br>\
							<label>Offset:</label><input type="text" size=4 id="r_offset_' + nextRowId + '" class="nonreq"></input><br>\
							<label>Depth:</label><input type="text" size=4 id="r_depth_' + nextRowId + '" class="nonreq"></input><br>\
						</td>\
							\
						<td>\
							<label>Rel positioning</label><br>\
							<label>Distance:</label><input type="text" size=4 id="r_distance_' + nextRowId + '" class="nonreq"></input><br>\
							<label>Within:</label><input type="text" size=4 id="r_within_' + nextRowId + '" class="nonreq"></input>\
						</td>\
						<td>\
							<input value="Delete row" type="button" class="btn btn-danger" onClick="$(this).parent().parent().remove();"></td>\
                    </tr>\
                    ';

	$("#t_contents tbody").append(content_row) ;
	nextRowId += 1;
}

function addDnsRow(){
	var dns_row = '\
        <tr id="cr_' + nextRowId + '" class=tr_dnsrow">\
            <\
			<td><label>Domain</label></td>\
			<td colspan="3"><input class="req" type="text" style="width: 100%;" id=r_domain_' + nextRowId +'> </td>\
			<td><label>Flags:</label><br>\
			<input type="checkbox" id="r_query_' + nextRowId + '"><label for="r_query_' + nextRowId + '">Query only</label> <br> \
			<input type="checkbox" id="r_suffix_' + nextRowId + '"><label for="r_suffix_' + nextRowId + '">Allow suffix</label><br>\
			<input type="checkbox" id="r_answer_' + nextRowId + '" style="visibility:hidden"><label style="visibility:hidden" for="r_answer_' + nextRowId + '">Answer only</label> \
			</td>\
            <td><input value="Delete row" type="button" class="btn btn-danger" onClick="$(this).parent().parent().remove();"></td>\
	    ';


	$("#t_contents tbody").append(dns_row) ;
	nextRowId += 1;
}



function addFiledataRow(){
	var filedata_row = '\
                    <tr id="cr_' + nextRowId + '" class=tr_filedatarow">\
                        <td colspan="5"><label>filedata</label></td>\
                        <td><input value="Delete row" type="button" class="btn btn-danger" onClick="$(this).parent().parent().remove();"></td>\
					</td>\
	';
	$("#t_contents tbody").append(filedata_row) ;
	nextRowId += 1;
}

function addCustomRow(){
	var custom_row = '\
                    <tr id="cr_' + nextRowId + '" class=tr_customrow">\
                        <td><label>Custom ruletext:</label></td>\
                        <td colspan="4"><input class="req" type="text" style="width: 100%;" id=r_custom_' + nextRowId +'> </td>\
                        <td><input value="Delete row" type="button" class="btn btn-danger" onClick="$(this).parent().parent().remove();"></td>\
					</td>\
	';
	$("#t_contents tbody").append(custom_row) ;
	nextRowId += 1;
}

function addPcreRow(){
	var custom_row = '\
                    <tr id="cr_' + nextRowId + '" class=tr_pcrerow">\
                        <td><label>PCRE:</label></td>\
                        <td colspan="3"><input class="req" type="text" style="width: 100%;" id=r_pcre_' + nextRowId +'> </td>\
                        <td><div class="nonreq"><input type="checkbox" id="r_pcreneg_' + nextRowId +'"> <label for="r_pcreneg_' + nextRowId +'">negative match</label></div></td> \
                        <td><input value="Delete row" type="button" class="btn btn-danger" onClick="$(this).parent().parent().remove();"></td>\
					</td>\
	';
	$("#t_contents tbody").append(custom_row) ;
	nextRowId += 1;
}
